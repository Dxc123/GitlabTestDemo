//
//  AppDelegate.h
//  gitlabTestDemo
//
//  Created by Dxc_iOS on 2018/8/8.
//  Copyright © 2018年 Dxc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

